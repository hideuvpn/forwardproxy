# Secure forward proxy plugin for the Caddy web server

[![Build Status](https://travis-ci.org/caddyserver/forwardproxy.svg?branch=master)](https://travis-ci.org/caddyserver/forwardproxy)  
[![Join the chat at https://gitter.im/forwardproxy/Lobby](https://badges.gitter.im/forwardproxy/Lobby.svg)](https://gitter.im/forwardproxy/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)


This plugin enables [Caddy](https://caddyserver.com) to act as a forward proxy, with support for HTTP/2.0 and HTTP/1.1 requests. HTTP/2.0 will usually improve performance due to multiplexing.

Forward proxy plugin includes common features like Access Control Lists and authentication, as well as some unique features to assist with security and privacy. Default configuration of forward proxy is compliant with existing HTTP standards, but some features force plugin to exhibit non-standard but non-breaking behavior to preserve privacy.

Probing resistance—one of the signature features of this plugin—attempts to hide the fact that your webserver is also a forward proxy, helping the proxy to stay under the radar. Eventually, forwardproxy plugin implemented a simple *reverse* proxy (`upstream https://user:password@next-hop.com` in Caddyfile) just so users may take advantage of `probe_resistance` when they need a reverse proxy (for example, to build a chain of proxies). Reverse proxy implementation will stay simple, and if you need a powerful reverse proxy, look into Caddy's standard `proxy` directive.

For a complete list of features and their usage, see Caddyfile syntax:

## Caddyfile Syntax (Server Configuration)

The simplest way to enable the forward proxy without authentication just include the `forward_proxy` directive in your Caddyfile. However, this allows anyone to use your server as a proxy, which might not be desirable.

The `forward_proxy` directive has no default order and must be used within a `route` directive to explicitly specify its order of evaluation. In the Caddyfile the addresses must start with `:443` for the `forward_proxy` to work for proxy requests of all origins.

Here's an example of all properties in use (note that the syntax is subject to change):

```
:443, example.com
route {
  forward_proxy {
    basic_auth user1 0NtCL2JPJBgPPMmlPcJ
    basic_auth user2 密码
    ports     80 443
    hide_ip
    hide_via
    probe_resistance secret-link-kWWL9Q.com # alternatively you can use a real domain, such as caddyserver.com
    serve_pac        /secret-proxy.pac
    dial_timeout     30
    upstream         https://user:password@extra-upstream-hop.com
    acl {
      allow     *.caddyserver.com
      deny      192.168.1.1/32 192.168.0.0/16 *.prohibitedsite.com *.localhost
      allow     ::1/128 8.8.8.8 github.com *.github.io
      allow_file /path/to/whitelist.txt
      deny_file  /path/to/blacklist.txt
      allow     all
      deny      all # unreachable rule, remaining requests are matched by `allow all` above
    }
  }
  file_server
}
```

(The square brackets `[ ]` indicate values you should replace; do not actually include the brackets.)

##### Security

- **basic_auth [user] [password]**  
Sets basic HTTP auth credentials. This property may be repeated multiple times. Note that this is different from Caddy's built-in `basic_auth` directive. BE SURE TO CHECK THE NAME OF THE SITE THAT IS REQUESTING CREDENTIALS BEFORE YOU ENTER THEM.  
_Default: no authentication required._

- **probe_resistance [secretlink.tld]**  
Attempts to hide the fact that the site is a forward proxy.
Proxy will no longer respond with "407 Proxy Authentication Required" if credentials are incorrect or absent,
and will attempt to mimic a generic Caddy web server as if the forward proxy is not enabled.  
Probing resistance works (and makes sense) only if `basic_auth` is set up.
To use your proxy with probe resistance, supply your `basic_auth` credentials to your client configuration.
If your proxy client(browser, operating system, browser extension, etc)
allows you to preconfigure credentials, and sends credentials preemptively, you do not need secret link.  
If your proxy client does not preemptively send credentials, you will have to visit your secret link in your browser to trigger the authentication.
Make sure that specified domain name is visitable, does not contain uppercase characters, does not start with dot, etc.
Only this address will trigger a 407 response, prompting browsers to request credentials from user and cache them for the rest of the session.
_Default: no probing resistance._

##### Privacy

- **hide_ip**  
If set, forwardproxy will not add user's IP to "Forwarded:" header.  
WARNING: there are other side-channels in your browser, that you might want to eliminate, such as WebRTC, see [here](https://www.ivpn.net/knowledgebase/158/My-IP-is-being-leaked-by-WebRTC-How-do-I-disable-it.html) how to disable it.  
_Default: no hiding; `Forwarded: for="useraddress"` will be sent out._

- **hide_via**  
If set, forwardproxy will not add Via header, and prevents simple way to detect proxy usage.  
WARNING: there are other side-channels to determine this.  
_Default: no hiding; Header in form of `Via: 2.0 caddy` will be sent out._

##### Access Control

- **ports [integer] [integer]...**  
Specifies ports forwardproxy will whitelist for all requests. Other ports will be forbidden.  
_Default: no restrictions._

- **acl {  
&nbsp;&nbsp;&nbsp;&nbsp;acl_directive  
&nbsp;&nbsp;&nbsp;&nbsp;...  
&nbsp;&nbsp;&nbsp;&nbsp;acl_directive  
}**  
Specifies **order** and rules for allowed destination IP networks, IP addresses and hostnames.
The hostname in each forwardproxy request will be resolved to an IP address,
and caddy will check the IP address and hostname against the directives in order until a directive matches the request.
acl_directive may be:
  - **allow [ip or subnet or hostname] [ip or subnet or hostname]...**
  - **allow_file /path/to/whitelist.txt**
  - **deny [ip or subnet or hostname] [ip or subnet or hostname]...**
  - **deny_file /path/to/blacklist.txt**

  If you don't want unmatched requests to be subject to the default policy, you could finish
  your acl rules with one of the following to specify action on unmatched requests:
  - **allow all**
  - **deny all**
  
  For hostname, you can specify `*.` as a prefix to match domain and subdomains. For example,
  `*.caddyserver.com` will match `caddyserver.com`, `subdomain.caddyserver.com`, but not `fakecaddyserver.com`.
  Note that hostname rules, matched early in the chain, will override later IP rules,
  so it is advised to put IP rules first, unless domains are highly trusted and should override the
  IP rules. Also note that domain-based blacklists are easily circumventable by directly specifying the IP.  
  For `allow_file`/`deny_file` directives, syntax is the same, and each entry must be separated by newline.  
  This policy applies to all requests except requests to the proxy's own domain and port.
  Whitelisting/blacklisting of ports on per-host/IP basis is not supported.  
_Default policy:_  
acl {  
&nbsp;&nbsp;&nbsp;&nbsp;deny 10.0.0.0/8 127.0.0.0/8 172.16.0.0/12 192.168.0.0/16 ::1/128 fe80::/10  
&nbsp;&nbsp;&nbsp;&nbsp;allow all  
}  
_Default deny rules intend to prohibit access to localhost and local networks and may be expanded in future._

##### Timeouts

- **dial_timeout [integer]**  
Sets timeout (in seconds) for establishing TCP connection to target website. Affects all requests.  
_Default: 20 seconds._

##### Other

- **serve_pac [/path.pac]**  
Generate (in-memory) and serve a [Proxy Auto-Config](https://en.wikipedia.org/wiki/Proxy_auto-config) file on given path. If no path is provided, the PAC file will be served at `/proxy.pac`. NOTE: If you enable probe_resistance, your PAC file should also be served at a secret location; serving it at a predictable path can easily defeat probe resistance.  
_Default: no PAC file will be generated or served by Caddy (you still can manually create and serve proxy.pac like a regular file)._

- **upstream [`https://username:password@upstreamproxy.site:443`]**  
Sets upstream proxy to route all forwardproxy requests through it.
This setting does not affect non-forwardproxy requests nor requests with wrong credentials.
Upstream is incompatible with `acl` and `ports` subdirectives.  
Supported schemes to remote host: https.  
Supported schemes to localhost: socks5, http, https (certificate check is ignored).  
_Default: no upstream proxy._

## Get forwardproxy
#### Download prebuilt binary
Binaries are at https://caddyserver.com/download  
Don't forget to add `http.forwardproxy` plugin.

#### Build from source

0. Install latest Golang 1.12 or above and set export GO111MODULE=on
1. ```bash
   go install github.com/caddyserver/forwardproxy/cmd/caddy
   ```   
   Built `caddy` binary will be stored in $GOPATH/bin.  

## Client Configuration

Please be aware that client support varies widely, and there are edge cases where clients may not use the proxy when it should or could. It's up to you to be aware of these limitations.

The basic configuration is simply to use your site address and port (usually for all protocols - HTTP, HTTPS, etc). You can also specify the .pac file if you enabled that.

Read [this blog post](https://sfrolov.io/2017/08/secure-web-proxy-client-en) about how to configure your specific client.

## License

Licensed under the [Apache License](LICENSE)

## Disclaimers

USE AT YOUR OWN RISK. THIS IS DELIVERED AS-IS. By using this software, you agree and assert that authors, maintainers, and contributors of this software are not responsible or liable for any risks, costs, or problems you may encounter. Consider your threat model and be smart. If you find a flaw or bug, please submit a patch and help make things better!

Initial version of this plugin was developed by Google. This is not an official Google product.


xcaddy build --with github.com/caddyserver/forwardproxy@caddy2=gitlab.com/hideuvpn/forwardproxy@main

go install github.com/caddyserver/xcaddy/cmd/xcaddy@latest
~/go/bin/xcaddy build --with github.com/caddyserver/forwardproxy@caddy2=github.com/klzgrad/forwardproxy@naive

### install golang
wget https://dl.google.com/go/go1.19.4.linux-amd64.tar.gz
sudo tar -xf go1.19.4.linux-amd64.tar.gz -C /usr/local
vim /etc/profile
export PATH=$PATH:/usr/local/go/bin
source /etc/profile
### install xcaddy
sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/xcaddy/gpg.key' | gpg --dearmor -o /usr/share/keyrings/caddy-xcaddy-archive-keyring.gpg
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/xcaddy/debian.deb.txt' | tee /etc/apt/sources.list.d/caddy-xcaddy.list
sudo apt update
sudo apt install xcaddy

### install forwardproxy
xcaddy build --with github.com/caddyserver/forwardproxy@caddy2=gitlab.com/hideuvpn/forwardproxy@main

xcaddy build --with github.com/caddyserver/forwardproxy@caddy2=./forwardproxy

### TODO
1. 集成naive和https到同一个项目


### init
mkdir /opt/caddy/ && cd /opt/caddy
wget https://gitlab.com/hideuvpn/forwardproxy/-/raw/main/caddy -O caddy && chmod +x caddy
wget https://gitlab.com/hideuvpn/forwardproxy/-/raw/main/html.zip -O html.zip && apt install -y unzip && unzip html.zip && mv -f ./html /var/www
NODE_ID=71 && wget http://hucdn.hideu.pw/cdn/dotfiles/caddy.json -O caddy.json && sed -i -e "s/####/${NODE_ID}/g" caddy.json

echo "[program:caddy_server]
command = /opt/caddy/caddy run --config /opt/caddy/caddy.json
directory = /opt/caddy/
user = root
autostart = true
autorestart = true
stdout_logfile = /var/log/supervisor/caddy_server.log
stderr_logfile = /var/log/supervisor/caddy_server.err">/etc/supervisor/conf.d/caddy_server.conf


wget http://hucdn.hideu.pw/cdn/dotfiles/max.sh -O max.sh && bash max.sh 71 1

wget http://hucdn.hideu.pw/cdn/dotfiles/max.sh -O max.sh && bash max.sh 71 5

docker stop 7ab2fc1211d5 && kill -9  492 && supervisorctl update

docker stop 404be9c03d83 d187a567ccbf && supervisorctl update && docker rm 404be9c03d83 d187a567ccbf

### socks5
{
  "n-a-jp.crispcdn.com":"socks5://hello:world..@203.83.244.49:11080",
  "n-b-jp.crispcdn.com":"socks5://hello:world..@127.0.0.1:11080",
  "n-c-jp.crispcdn.com":"socks5://hello:world..@127.0.0.1:11080",
  "n-d-jp.crispcdn.com":"socks5://hello:world..@127.0.0.1:11080",
}
n-a-jp.crispcdn.com
h-a-jp.crispcdn.com
h-c-jp.crispcdn.com

{
"h-a-jp.crispcdn.com":"123.123.123.123",
"h-b-jp.crispcdn.com":"123.123.123.123",
"h-c-jp.crispcdn.com":"123.123.123.123",
"h-d-jp.crispcdn.com":"123.123.123.123",
}


docker run -d --name socks5 -p 11080:11080 -e PROXY_USER=hello -e PROXY_PASSWORD=world..  serjs/go-socks5-proxy
docker run -d --name socks5 -p 11080:11080 

curl -fsSL https://get.docker.com -o get-docker.sh  && sh get-docker.sh
mkdir /opt/docker_projects/
vim /opt/docker_projects/socks5.yml
```shell
version: '3.2'

services:
  socks5-go:
    restart: always
    image: maxleeup/socks5-go:1.0.0
    network_mode: "host"
    volumes:
      - /etc/localtime:/etc/localtime:ro
    ulimits:
      nproc: 1024000
      nofile:
        soft: 1024000
        hard: 1024000
```
docker compose -f /opt/docker_projects/socks5.yml up -d

45.124.136.249
121.50.43.32
121.50.44.95
121.50.43.108

"socks5_servers":{
  "n-a-jp.crispcdn.com": "socks5://45.124.136.249:11080",
  "n-b-jp.crispcdn.com": "socks5://121.50.43.32:11080",
  "n-c-jp.crispcdn.com": "socks5://121.50.44.95:11080",
  "n-d-jp.crispcdn.com": "socks5://121.50.43.108:11080"
},

"socks5_servers":{
"h-a-jp.crispcdn.com": "socks5://45.124.136.249:11080",
"h-b-jp.crispcdn.com": "socks5://121.50.43.32:11080",
"h-c-jp.crispcdn.com": "socks5://121.50.44.95:11080",
"h-d-jp.crispcdn.com": "socks5://121.50.43.108:11080"
},

in  :  103.174.87.149
th  :  119.59.105.41
vn  :  103.12.76.245
my  :  202.59.9.94

"socks5_servers":{
"n-a-in.crispcdn.com": "socks5://103.174.87.149:11080",
"n-a-th.crispcdn.com": "socks5://119.59.105.41:11080",
"n-a-vn.crispcdn.com": "socks5://103.12.76.245:11080",
"n-a-my.crispcdn.com": "socks5://202.59.9.94:11080"
},

"socks5_servers":{
"h-a-in.crispcdn.com": "socks5://103.174.87.149:11080",
"h-a-th.crispcdn.com": "socks5://119.59.105.41:11080",
"h-a-vn.crispcdn.com": "socks5://103.12.76.245:11080",
"h-a-my.crispcdn.com": "socks5://202.59.9.94:11080"
},

,202.59.9.94: [443, 9443]
,103.12.76.245: [443, 9443]
,119.59.105.41: [443, 9443]
,103.174.87.149: [443, 9443]

,45.124.136.249: [443, 9443]
,121.50.43.32: [443, 9443]
,121.50.44.95: [443, 9443]
,121.50.43.108: [443, 9443]